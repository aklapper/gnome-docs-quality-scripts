#!/bin/bash

# This code is licensed under CC0 1.0 Universal: https://creativecommons.org/publicdomain/zero/1.0/legalcode

# run this script as "script.sh > foo.csv"
# open foo.csv in a text editor
# replace two linebreaks \n\n by semicolon
# replace all linebreaks by whitespace
# replace all semicolons by linebreaks
# import csv file into libreoffice calc with linebreaks as separators
# sort ascending for first column
# Create a new column that says 1 checks if the previous row has the same value as the current, else 0.
# See http://guide2office.com/1270/how-to-remove-duplicate-rows-in-calc-ooo-libreoffice-neooffice/
# Basically this is about using a formular like =IF(A2=A1,1,0) and then dragging that down the column for all rows
# Find all ones with a 1 and fix them

cd GNOME/
for i in $( ls ); do
# for i in four-in-a-row gnome-nibbles iagno; do
	if [ -d $i ]; then
		cd $i
		if [ -d "help" ]; then
			cd help
			for j in $( ls ); do
				if [ -d $j ]; then
					cd $j
					if [ -d "figures" ]; then
						cd figures
						for k in $( ls ); do
							sha256sum $k
							echo $i"/"$j
							echo ""
						done
						cd ..
					fi
					cd ..
				fi
			done
			cd ..
#		elif [ -d "docs" ]; then # 
		fi
		cd ..
	fi
done
cd ..
