#!/bin/bash

# This code is licensed under CC0 1.0 Universal: https://creativecommons.org/publicdomain/zero/1.0/legalcode
# Might require to have the yelp-tools package installed

cd GNOME/
for i in $( ls ); do
# for i in four-in-a-row gnome-nibbles iagno; do
  if [ -d $i ]; then
    cd $i
    if [ -d "help" ]; then
      cd help
      if [ -d "C" ]; then
        cd C/
        if [ -e index.page ]; then # mallard
          echo "====" $i "===="
          echo "* ids:"
          yelp-check ids *.page
          echo "* links:"
          yelp-check links *.page
          echo "* media:"
          yelp-check media *.page
          echo "* orphans:"
          yelp-check orphans *.page
          echo "* validate:"
          yelp-check validate *.page
        fi
        cd ..
      fi
      cd ..
    fi
    cd ..
  fi
done
cd ..
