#!/bin/bash

# This code is licensed under CC0 1.0 Universal: https://creativecommons.org/publicdomain/zero/1.0/legalcode
# Check for each image file in /C/images/ whether it is actually used in a Mallard or Docbook file

cd GNOME/
for i in $( ls ); do
# for i in four-in-a-row gnome-nibbles iagno; do
  if [ -d $i ]; then
    cd $i
    if [ -d "help" ]; then
      cd help
      if [ -d "C" ]; then
        cd C/
        if [ -d "figures" ]; then
          cd figures
          for j in $( ls ); do
            if [ -f ../index.page ]; then # mallard
              if ! grep -qr $j ../*.page; then
                echo $i"/"$j" not used?";
              fi
            fi
            if [ -f ../index.docbook ]; then
              a1=$(grep -qr "$j" ../index.docbook)
              a2=$(grep -qr "$j" ../*.xml)
              if ! ( $a1 && $a2 ); then
                echo $i"/"$j" not used?";
              fi
            fi
          done
          cd ..
        fi
        cd ..
      fi
      cd ..
    fi
    cd ..
  fi
done
cd ..
