#!/bin/bash

# This code is licensed under CC0 1.0 Universal: https://creativecommons.org/publicdomain/zero/1.0/legalcode

cd GNOME/
for i in $( ls ); do
# for i in four-in-a-row gnome-nibbles iagno; do
  if [ -d $i ]; then
    cd $i
    if [ -d "help" ]; then
      cd help
      for j in $( ls ); do
        if [ -d $j ]; then
          cd $j
          if [ -d "figures" ]; then # most common directory name
            cd figures
            echo "=== $i/help/$j/figures: ==="
            if [ -d "../../C/figures/" ]; then #
              comm -13 <(ls ../../C/figures/) <(ls .)
            else
              ls
            fi
            cd ..
          elif [ -d "media" ]; then # directory name in baobab etc.
            cd media
            echo "=== $i/help/$j/media: ==="
            if [ -d "../../C/media/" ]; then #
              comm -13 <(ls ../../C/media/) <(ls .)
            else
              ls
            fi
            cd ..
          fi
          cd ..
        fi
      done
      cd ..
    fi
    cd ..
  fi
done
cd ..
# Many ways to do this, e.g. "diff -qr C/figures fr/figures | sort"
