#!/bin/bash

# This code is licensed under CC0 1.0 Universal: https://creativecommons.org/publicdomain/zero/1.0/legalcode
# gtxml must be installed: https://github.com/pyg3t/pyg3t

echo "Checking translation files in the Git repository."
echo ""
echo "Important note: The translation files in Git can be outdated and hence include English strings"
echo "which are not used anymore. Best to check against the latest version on https://l10n.gnome.org/"
echo "(which is automatically updated) before filing a bug report."
sleep 5;
cd GNOME/
for i in $( ls ); do
# for i in evolution gnome-games nautilus; do
  if [ -d $i ]; then
    cd $i
    if [ -d "help" ]; then
      cd help
      echo "==="$i"==="
      for j in $( ls ); do
        if [ -d $j ] && [ ! $j == "C" ]; then
          cd $j
            if [ -e $j.po ]; then # some folders are actually empty
              gtxml $j.po
            fi
          cd ..
        fi
      done
      cd ..
    fi
    cd ..
  fi
done
cd ..
